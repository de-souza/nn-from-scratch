# NN From Scratch (Python Implementation)

This is the Python implementation of the neural network.

## Requirements

- Python 3
- PyTorch (for matrix creation and multiplication)

## Running the Unit Tests

1. Open a shell in this directory
2. Run `python -m unittest` to run the unit tests

## License

This project is licensed under the [Apache License 2.0](https://gitlab.com/de-souza/nn-from-scratch/blob/master/LICENSE).
