import torch


def matmul(mat_a, mat_b):
    rows_a, columns_a = mat_a.shape
    rows_b, columns_b = mat_b.shape
    if columns_a != rows_b:
        raise ValueError("dimension mismatch")
    mat_c = torch.zeros(rows_a, columns_b)
    for i in range(rows_a):
        for j in range(columns_b):
            for k in range(columns_a):
                mat_c[i, j] += mat_a[i, k] * mat_b[k, j]
    return mat_c
