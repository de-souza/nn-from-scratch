using NNFromScratch

@testset "correct result" begin
    mat_a = rand(5, 3)
    mat_b = rand(3, 2)
    @test NNFromScratch.matmul(mat_a, mat_b) == mat_a * mat_b
end

@testset "dimension mismatch" begin
    mat_a = rand(2, 3)
    mat_b = rand(2, 3)
    @test_throws DimensionMismatch NNFromScratch.matmul(mat_b, mat_a)
end
