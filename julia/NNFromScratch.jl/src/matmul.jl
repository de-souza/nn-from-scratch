function matmul(mat_a::AbstractMatrix, mat_b::AbstractMatrix)
    rows_a, columns_a = size(mat_a)
    rows_b, columns_b = size(mat_b)
    columns_a == rows_b || throw(DimensionMismatch("matrices must match"))
    mat_c = zeros(rows_a, columns_b)
    for k = 1:columns_a, j = 1:columns_b, i = 1:rows_a
        mat_c[i, j] += mat_a[i, k] * mat_b[k, j]
    end
    mat_c
end
