using Statistics: std, mean
using Flux.Data.MNIST

struct Dataset
    x_train::Array{Float32}
    y_train::Vector{Int64}
    x_valid::Array{Float32}
    y_valid::Vector{Int64}
end

function unnest(v::AbstractVector{<:AbstractArray})
    outersize = length(v)
    innersize = size(v[begin])
    concatenated = hcat(v...)
    reshape(concatenated, (innersize..., outersize))
end

function getdata()
    x_train = unnest(MNIST.images())
    y_train = MNIST.labels()
    x_valid = unnest(MNIST.images(:test))
    y_valid = MNIST.labels(:test)
    Dataset(x_train, y_train, x_valid, y_valid)
end

function innerflatten(A::AbstractArray)
    outersize = size(A)[end]
    innersize = size(A)[begin:end-1]
    reshape(A, (prod(innersize), outersize))
end

function innerflatten(data::Dataset)
    x_train = innerflatten(data.x_train)
    y_train = data.y_train
    x_valid = innerflatten(data.x_valid)
    y_valid = data.y_valid
    Dataset(x_train, y_train, x_valid, y_valid)
end

@. normalize(
    A::AbstractArray{<:AbstractFloat},
    mean::AbstractFloat,
    std::AbstractFloat,
) = (A - mean) / std

function normalize(data::Dataset)
    train_mean = mean(data.x_train)
    train_std = std(data.x_train)
    x_train = normalize(data.x_train, train_mean, train_std)
    y_train = data.y_train
    x_valid = normalize(data.x_valid, train_mean, train_std)
    y_valid = data.y_valid
    Dataset(x_train, y_train, x_valid, y_valid)
end

getprocesseddata = normalize ∘ innerflatten ∘ getdata
