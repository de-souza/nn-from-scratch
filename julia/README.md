# NN From Scratch (Julia Implementation)

This is the Julia implementation of the neural network.

## Requirements

- Julia >= 1.3

## Installing Dependencies

1. Open the Julia REPL in the `NNFromScratch.jl` directory
2. Press `]` to enter Pkg mode
3. Type `activate .` to activate the project
4. Type `instantiate` to install its dependencies

## Running the Unit Tests

1. Open the Julia REPL in the `NNFromScratch.jl` directory
2. Press `]` to enter Pkg mode
3. Type `activate .` to activate the project
3. Type `test` to run the unit tests

## License

This project is licensed under the [Apache License 2.0](https://gitlab.com/de-souza/nn-from-scratch/blob/master/LICENSE).
